import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

const Root = () => {
	return (
		<App url={'http://pagedesigngroup.dev/wp-json/wp/v2/portfolio-api?_embed'} />
	)
}

ReactDOM.render(<Root />, document.getElementById('root'));
registerServiceWorker();
