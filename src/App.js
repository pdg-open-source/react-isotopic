import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import './Masonry.css';
import Header from './components/Header';
import Filter from './components/Filter';
import PortfolioList from './components/PortfolioList';
//import posts from './data/posts';

class App extends Component {
    constructor() {
        super();
        this.state = {
            posts: [],
            shownPosts: []
        }
    }

    handleFilter = (catFilter) => {
        const newPosts = this.state.posts.filter(post => {
            if(!post._embedded['wp:term']) {
                return
            }
            return post._embedded['wp:term'][0][0]['slug'] == catFilter
        })
        this.setState({
            shownPosts: newPosts
        })

    }

    componentDidMount() {
        const posts = []
        fetch(this.props.url)
            .then(response => response.json())
            .then(json => {
                this.setState({ posts: json, shownPosts: json });
            });

    }

    render() {
        return (
            <div className="App">

                <Header headline="Our Work" tagline="Show & Tell" />
                
                <Filter handleFilter={this.handleFilter} />

                <PortfolioList posts={this.state.shownPosts} />
                
          </div>
        );
    }
}

export default App;
