const posts = [
    {
        "id": 1657,
        "date": "2017-03-27T22:48:27",
        "date_gmt": "2017-03-27T22:48:27",
        "guid": {
            "rendered": "http://pdg16.wpengine.com/?post_type=portfolio&#038;p=1657"
        },
        "modified": "2017-04-14T17:24:31",
        "modified_gmt": "2017-04-14T17:24:31",
        "slug": "medical-school-magazine",
        "status": "publish",
        "type": "portfolio",
        "link": "https://pagedesigngroup.com/portfolio/medical-school-magazine/",
        "title": {
            "rendered": "Medical School Magazine"
        },
        "content": {
            "rendered": "<p>Medicine Magazine is printed three times per year and serves the Academic Faculty, Students, Staff and Alumni of the UC Davis School of Medicine.</p>\n",
            "protected": false
        },
        "excerpt": {
            "rendered": "<p>Medicine Magazine is printed three times per year and serves the Academic Faculty, Students, Staff and Alumni of the UC Davis School of Medicine.</p>\n",
            "protected": false
        },
        "featured_media": 1659,
        "parent": 0,
        "template": "",
        "tags": [
            18,
            53
        ],
        "acf": {},
        "_links": {
            "self": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api/1657"
                }
            ],
            "collection": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api"
                }
            ],
            "about": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/types/portfolio"
                }
            ],
            "version-history": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api/1657/revisions"
                }
            ],
            "wp:featuredmedia": [
                {
                    "embeddable": true,
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/media/1659"
                }
            ],
            "wp:attachment": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/media?parent=1657"
                }
            ],
            "wp:term": [
                {
                    "taxonomy": "post_tag",
                    "embeddable": true,
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags?post=1657"
                }
            ],
            "curies": [
                {
                    "name": "wp",
                    "href": "https://api.w.org/{rel}",
                    "templated": true
                }
            ]
        },
        "_embedded": {
            "wp:featuredmedia": [
                {
                    "id": 1659,
                    "date": "2017-03-27T22:47:47",
                    "slug": "medicine-magazine-2",
                    "type": "attachment",
                    "link": "https://pagedesigngroup.com/portfolio/medical-school-magazine/attachment/medicine-magazine-2/",
                    "title": {},
                    "author": 1,
                    "acf": [],
                    "caption": {},
                    "alt_text": "",
                    "media_type": "image",
                    "mime_type": "image/png",
                    "media_details": {},
                    "source_url": "https://pagedesigngroup.com/wp-content/uploads/Medicine-Magazine.png",
                    "_links": {}
                }
            ],
            "wp:term": [
                [
                    {
                        "id": 18,
                        "link": "https://pagedesigngroup.com/tag/education/",
                        "name": "Education",
                        "slug": "education",
                        "taxonomy": "post_tag",
                        "_links": {
                            "self": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags/18"
                                }
                            ],
                            "collection": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags"
                                }
                            ],
                            "about": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/taxonomies/post_tag"
                                }
                            ],
                            "wp:post_type": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/posts?tags=18"
                                },
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api?tags=18"
                                }
                            ],
                            "curies": [
                                {
                                    "name": "wp",
                                    "href": "https://api.w.org/{rel}",
                                    "templated": true
                                }
                            ]
                        }
                    },
                    {
                        "id": 53,
                        "link": "https://pagedesigngroup.com/tag/magazine/",
                        "name": "Magazine",
                        "slug": "magazine",
                        "taxonomy": "post_tag",
                        "_links": {
                            "self": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags/53"
                                }
                            ],
                            "collection": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags"
                                }
                            ],
                            "about": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/taxonomies/post_tag"
                                }
                            ],
                            "wp:post_type": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/posts?tags=53"
                                },
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api?tags=53"
                                }
                            ],
                            "curies": [
                                {
                                    "name": "wp",
                                    "href": "https://api.w.org/{rel}",
                                    "templated": true
                                }
                            ]
                        }
                    }
                ]
            ]
        }
    },
    {
        "id": 1613,
        "date": "2017-03-21T19:17:25",
        "date_gmt": "2017-03-21T19:17:25",
        "guid": {
            "rendered": "http://pdg16.wpengine.com/?post_type=portfolio&#038;p=1613"
        },
        "modified": "2017-09-13T17:19:35",
        "modified_gmt": "2017-09-13T17:19:35",
        "slug": "ecosystem-benefits-one-sheet",
        "status": "publish",
        "type": "portfolio",
        "link": "https://pagedesigngroup.com/portfolio/ecosystem-benefits-one-sheet/",
        "title": {
            "rendered": "Ecosystem Benefits One Sheet"
        },
        "content": {
            "rendered": "<p>The proposed Sites Reservoir project will benefit California by providing another source for precious water. This sheet provides a quick reference guide highlighting the positive environmental and economic benefits of this huge infrastructure project.</p>\n",
            "protected": false
        },
        "excerpt": {
            "rendered": "<p>The proposed Sites Reservoir project will benefit California by providing another source for precious water. This sheet provides a quick reference guide highlighting the positive environmental and economic benefits of this huge infrastructure project.</p>\n",
            "protected": false
        },
        "featured_media": 1615,
        "parent": 0,
        "template": "",
        "tags": [
            18,
            40,
            34,
            41
        ],
        "acf": {
            "client": "Sites Project Authority",
            "full_size_image": [
                {
                    "ID": 1831,
                    "id": 1831,
                    "title": "sites_onesheet_1",
                    "filename": "sites_onesheet_1.png",
                    "url": "https://pagedesigngroup.com/wp-content/uploads/sites_onesheet_1.png",
                    "alt": "",
                    "author": "1",
                    "description": "",
                    "caption": "",
                    "name": "sites_onesheet_1",
                    "date": "2017-09-13 17:19:22",
                    "modified": "2017-09-13 17:19:31",
                    "mime_type": "image/png",
                    "type": "image",
                    "icon": "https://pagedesigngroup.com/wp-includes/images/media/default.png",
                    "width": 854,
                    "height": 540,
                    "sizes": {
                        "thumbnail": "https://pagedesigngroup.com/wp-content/uploads/sites_onesheet_1-150x150.png",
                        "thumbnail-width": 150,
                        "thumbnail-height": 150,
                        "medium": "https://pagedesigngroup.com/wp-content/uploads/sites_onesheet_1-220x139.png",
                        "medium-width": 220,
                        "medium-height": 139,
                        "medium_large": "https://pagedesigngroup.com/wp-content/uploads/sites_onesheet_1-768x486.png",
                        "medium_large-width": 768,
                        "medium_large-height": 486,
                        "large": "https://pagedesigngroup.com/wp-content/uploads/sites_onesheet_1.png",
                        "large-width": 854,
                        "large-height": 540,
                        "large2x": "https://pagedesigngroup.com/wp-content/uploads/sites_onesheet_1.png",
                        "large2x-width": 854,
                        "large2x-height": 540,
                        "hero": "https://pagedesigngroup.com/wp-content/uploads/sites_onesheet_1.png",
                        "hero-width": 854,
                        "hero-height": 540,
                        "medium2x": "https://pagedesigngroup.com/wp-content/uploads/sites_onesheet_1-440x278.png",
                        "medium2x-width": 440,
                        "medium2x-height": 278,
                        "story-tmb": "https://pagedesigngroup.com/wp-content/uploads/sites_onesheet_1-300x300.png",
                        "story-tmb-width": 300,
                        "story-tmb-height": 300,
                        "hero-m": "https://pagedesigngroup.com/wp-content/uploads/sites_onesheet_1-700x443.png",
                        "hero-m-width": 700,
                        "hero-m-height": 443
                    }
                }
            ],
            "file_download": false,
            "website_url": "",
            "video_truefalse": false
        },
        "_links": {
            "self": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api/1613"
                }
            ],
            "collection": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api"
                }
            ],
            "about": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/types/portfolio"
                }
            ],
            "version-history": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api/1613/revisions"
                }
            ],
            "wp:featuredmedia": [
                {
                    "embeddable": true,
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/media/1615"
                }
            ],
            "wp:attachment": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/media?parent=1613"
                }
            ],
            "wp:term": [
                {
                    "taxonomy": "post_tag",
                    "embeddable": true,
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags?post=1613"
                }
            ],
            "curies": [
                {
                    "name": "wp",
                    "href": "https://api.w.org/{rel}",
                    "templated": true
                }
            ]
        },
        "_embedded": {
            "wp:featuredmedia": [
                {
                    "id": 1615,
                    "date": "2017-03-21T19:36:36",
                    "slug": "sites",
                    "type": "attachment",
                    "link": "https://pagedesigngroup.com/portfolio/ecosystem-benefits-one-sheet/attachment/sites/",
                    "title": {
                        "rendered": "sites"
                    },
                    "author": 1,
                    "acf": [],
                    "caption": {
                        "rendered": ""
                    },
                    "alt_text": "",
                    "media_type": "image",
                    "mime_type": "image/png",
                    "media_details": {
                        "width": 370,
                        "height": 501,
                        "file": "sites.png",
                        "sizes": {
                            "thumbnail": {
                                "file": "sites-150x150.png",
                                "width": 150,
                                "height": 150,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/sites-150x150.png"
                            },
                            "medium": {
                                "file": "sites-162x220.png",
                                "width": 162,
                                "height": 220,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/sites-162x220.png"
                            },
                            "medium2x": {
                                "file": "sites-325x440.png",
                                "width": 325,
                                "height": 440,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/sites-325x440.png"
                            },
                            "story-tmb": {
                                "file": "sites-300x300.png",
                                "width": 300,
                                "height": 300,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/sites-300x300.png"
                            },
                            "full": {
                                "file": "sites.png",
                                "width": 370,
                                "height": 501,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/sites.png"
                            }
                        },
                        "image_meta": {
                            "aperture": "0",
                            "credit": "",
                            "camera": "",
                            "caption": "",
                            "created_timestamp": "0",
                            "copyright": "",
                            "focal_length": "0",
                            "iso": "0",
                            "shutter_speed": "0",
                            "title": "",
                            "orientation": "0",
                            "keywords": []
                        }
                    },
                    "source_url": "https://pagedesigngroup.com/wp-content/uploads/sites.png",
                    "_links": {
                        "self": [
                            {
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/media/1615"
                            }
                        ],
                        "collection": [
                            {
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/media"
                            }
                        ],
                        "about": [
                            {
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/types/attachment"
                            }
                        ],
                        "author": [
                            {
                                "embeddable": true,
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/users/1"
                            }
                        ],
                        "replies": [
                            {
                                "embeddable": true,
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/comments?post=1615"
                            }
                        ]
                    }
                }
            ],
            "wp:term": [
                [
                    {
                        "id": 18,
                        "link": "https://pagedesigngroup.com/tag/education/",
                        "name": "Education",
                        "slug": "education",
                        "taxonomy": "post_tag",
                        "_links": {
                            "self": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags/18"
                                }
                            ],
                            "collection": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags"
                                }
                            ],
                            "about": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/taxonomies/post_tag"
                                }
                            ],
                            "wp:post_type": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/posts?tags=18"
                                },
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api?tags=18"
                                }
                            ],
                            "curies": [
                                {
                                    "name": "wp",
                                    "href": "https://api.w.org/{rel}",
                                    "templated": true
                                }
                            ]
                        }
                    },
                    {
                        "id": 40,
                        "link": "https://pagedesigngroup.com/tag/environment/",
                        "name": "Environment",
                        "slug": "environment",
                        "taxonomy": "post_tag",
                        "_links": {
                            "self": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags/40"
                                }
                            ],
                            "collection": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags"
                                }
                            ],
                            "about": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/taxonomies/post_tag"
                                }
                            ],
                            "wp:post_type": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/posts?tags=40"
                                },
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api?tags=40"
                                }
                            ],
                            "curies": [
                                {
                                    "name": "wp",
                                    "href": "https://api.w.org/{rel}",
                                    "templated": true
                                }
                            ]
                        }
                    },
                    {
                        "id": 34,
                        "link": "https://pagedesigngroup.com/tag/water/",
                        "name": "Water",
                        "slug": "water",
                        "taxonomy": "post_tag",
                        "_links": {
                            "self": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags/34"
                                }
                            ],
                            "collection": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags"
                                }
                            ],
                            "about": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/taxonomies/post_tag"
                                }
                            ],
                            "wp:post_type": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/posts?tags=34"
                                },
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api?tags=34"
                                }
                            ],
                            "curies": [
                                {
                                    "name": "wp",
                                    "href": "https://api.w.org/{rel}",
                                    "templated": true
                                }
                            ]
                        }
                    },
                    {
                        "id": 41,
                        "link": "https://pagedesigngroup.com/tag/wildlife/",
                        "name": "Wildlife",
                        "slug": "wildlife",
                        "taxonomy": "post_tag",
                        "_links": {
                            "self": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags/41"
                                }
                            ],
                            "collection": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags"
                                }
                            ],
                            "about": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/taxonomies/post_tag"
                                }
                            ],
                            "wp:post_type": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/posts?tags=41"
                                },
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api?tags=41"
                                }
                            ],
                            "curies": [
                                {
                                    "name": "wp",
                                    "href": "https://api.w.org/{rel}",
                                    "templated": true
                                }
                            ]
                        }
                    }
                ]
            ]
        }
    },
    {
        "id": 1611,
        "date": "2017-03-21T18:45:54",
        "date_gmt": "2017-03-21T18:45:54",
        "guid": {
            "rendered": "http://pdg16.wpengine.com/?post_type=portfolio&#038;p=1611"
        },
        "modified": "2017-03-29T16:53:45",
        "modified_gmt": "2017-03-29T16:53:45",
        "slug": "time-of-use-video",
        "status": "publish",
        "type": "portfolio",
        "link": "https://pagedesigngroup.com/portfolio/time-of-use-video/",
        "title": {
            "rendered": "Time of Use Video"
        },
        "content": {
            "rendered": "<p>The Sacramento Municipal Utility District (SMUD) came to us with the idea for a new structure to their pricing model. We developed this animation to help teach people the basic points of these prices. This animation is a small part of an entire marketing campaign focused on this educational effort.</p>\n",
            "protected": false
        },
        "excerpt": {
            "rendered": "<p>The Sacramento Municipal Utility District (SMUD) came to us with the idea for a new structure to their pricing model. We developed this animation to help teach people the basic points of these prices. This animation is a small part of an entire marketing campaign focused on this educational effort.</p>\n",
            "protected": false
        },
        "featured_media": 1612,
        "parent": 0,
        "template": "",
        "tags": [
            18,
            124
        ],
        "acf": {
            "client": "SMUD",
            "full_size_image": false,
            "file_download": false,
            "website_url": "",
            "video_truefalse": true,
            "video": [
                {
                    "video_title": "Time of Use",
                    "video_url": "207541591"
                }
            ]
        },
        "_links": {
            "self": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api/1611"
                }
            ],
            "collection": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api"
                }
            ],
            "about": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/types/portfolio"
                }
            ],
            "version-history": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api/1611/revisions"
                }
            ],
            "wp:featuredmedia": [
                {
                    "embeddable": true,
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/media/1612"
                }
            ],
            "wp:attachment": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/media?parent=1611"
                }
            ],
            "wp:term": [
                {
                    "taxonomy": "post_tag",
                    "embeddable": true,
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags?post=1611"
                }
            ],
            "curies": [
                {
                    "name": "wp",
                    "href": "https://api.w.org/{rel}",
                    "templated": true
                }
            ]
        },
        "_embedded": {
            "wp:featuredmedia": [
                {
                    "id": 1612,
                    "date": "2017-03-21T18:45:33",
                    "slug": "time_of_use",
                    "type": "attachment",
                    "link": "https://pagedesigngroup.com/portfolio/time-of-use-video/attachment/time_of_use/",
                    "title": {
                        "rendered": "time_of_use"
                    },
                    "author": 1,
                    "acf": [],
                    "caption": {
                        "rendered": ""
                    },
                    "alt_text": "",
                    "media_type": "image",
                    "mime_type": "image/png",
                    "media_details": {
                        "width": 370,
                        "height": 251,
                        "file": "time_of_use.png",
                        "sizes": {
                            "thumbnail": {
                                "file": "time_of_use-150x150.png",
                                "width": 150,
                                "height": 150,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/time_of_use-150x150.png"
                            },
                            "medium": {
                                "file": "time_of_use-220x149.png",
                                "width": 220,
                                "height": 149,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/time_of_use-220x149.png"
                            },
                            "story-tmb": {
                                "file": "time_of_use-300x251.png",
                                "width": 300,
                                "height": 251,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/time_of_use-300x251.png"
                            },
                            "full": {
                                "file": "time_of_use.png",
                                "width": 370,
                                "height": 251,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/time_of_use.png"
                            }
                        },
                        "image_meta": {
                            "aperture": "0",
                            "credit": "",
                            "camera": "",
                            "caption": "",
                            "created_timestamp": "0",
                            "copyright": "",
                            "focal_length": "0",
                            "iso": "0",
                            "shutter_speed": "0",
                            "title": "",
                            "orientation": "0",
                            "keywords": []
                        }
                    },
                    "source_url": "https://pagedesigngroup.com/wp-content/uploads/time_of_use.png",
                    "_links": {
                        "self": [
                            {
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/media/1612"
                            }
                        ],
                        "collection": [
                            {
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/media"
                            }
                        ],
                        "about": [
                            {
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/types/attachment"
                            }
                        ],
                        "author": [
                            {
                                "embeddable": true,
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/users/1"
                            }
                        ],
                        "replies": [
                            {
                                "embeddable": true,
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/comments?post=1612"
                            }
                        ]
                    }
                }
            ],
            "wp:term": [
                [
                    {
                        "id": 18,
                        "link": "https://pagedesigngroup.com/tag/education/",
                        "name": "Education",
                        "slug": "education",
                        "taxonomy": "post_tag",
                        "_links": {
                            "self": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags/18"
                                }
                            ],
                            "collection": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags"
                                }
                            ],
                            "about": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/taxonomies/post_tag"
                                }
                            ],
                            "wp:post_type": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/posts?tags=18"
                                },
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api?tags=18"
                                }
                            ],
                            "curies": [
                                {
                                    "name": "wp",
                                    "href": "https://api.w.org/{rel}",
                                    "templated": true
                                }
                            ]
                        }
                    },
                    {
                        "id": 124,
                        "link": "https://pagedesigngroup.com/tag/electricity/",
                        "name": "electricity",
                        "slug": "electricity",
                        "taxonomy": "post_tag",
                        "_links": {
                            "self": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags/124"
                                }
                            ],
                            "collection": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags"
                                }
                            ],
                            "about": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/taxonomies/post_tag"
                                }
                            ],
                            "wp:post_type": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/posts?tags=124"
                                },
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api?tags=124"
                                }
                            ],
                            "curies": [
                                {
                                    "name": "wp",
                                    "href": "https://api.w.org/{rel}",
                                    "templated": true
                                }
                            ]
                        }
                    }
                ]
            ]
        }
    },
    {
        "id": 1599,
        "date": "2017-03-20T18:52:16",
        "date_gmt": "2017-03-20T18:52:16",
        "guid": {
            "rendered": "http://pdg16.wpengine.com/?post_type=portfolio&#038;p=1599"
        },
        "modified": "2017-03-30T16:42:31",
        "modified_gmt": "2017-03-30T16:42:31",
        "slug": "corporate-social-responsibility-report",
        "status": "publish",
        "type": "portfolio",
        "link": "https://pagedesigngroup.com/portfolio/corporate-social-responsibility-report/",
        "title": {
            "rendered": "Corporate Social Responsibility Report"
        },
        "content": {
            "rendered": "<p>The California Lottery is focused on having fun but they also have a commitment to providing support for responsible gaming, education initiatives and consumer protections related to playing their games. This colorful annual corporate social responsibility report outlines their extensive efforts to keep the public safe and the positive benefits the California Lottery provides the state.</p>\n",
            "protected": false
        },
        "excerpt": {
            "rendered": "<p>The California Lottery is focused on having fun but they also have a commitment to providing support for responsible gaming, education initiatives and consumer protections related to playing their games. This colorful annual corporate social responsibility report outlines their extensive efforts to keep the public safe and the positive benefits the California Lottery provides the [&hellip;]</p>\n",
            "protected": false
        },
        "featured_media": 1601,
        "parent": 0,
        "template": "",
        "tags": [],
        "acf": {
            "client": "California Lottery",
            "full_size_image": [
                {
                    "ID": 1600,
                    "id": 1600,
                    "title": "callottery_csr",
                    "filename": "callottery_csr.png",
                    "url": "https://pagedesigngroup.com/wp-content/uploads/callottery_csr.png",
                    "alt": "",
                    "author": "1",
                    "description": "",
                    "caption": "",
                    "name": "callottery_csr",
                    "date": "2017-03-20 18:50:47",
                    "modified": "2017-03-20 18:51:40",
                    "mime_type": "image/png",
                    "type": "image",
                    "icon": "https://pagedesigngroup.com/wp-includes/images/media/default.png",
                    "width": 912,
                    "height": 532,
                    "sizes": {
                        "thumbnail": "https://pagedesigngroup.com/wp-content/uploads/callottery_csr-150x150.png",
                        "thumbnail-width": 150,
                        "thumbnail-height": 150,
                        "medium": "https://pagedesigngroup.com/wp-content/uploads/callottery_csr-220x128.png",
                        "medium-width": 220,
                        "medium-height": 128,
                        "medium_large": "https://pagedesigngroup.com/wp-content/uploads/callottery_csr-768x448.png",
                        "medium_large-width": 768,
                        "medium_large-height": 448,
                        "large": "https://pagedesigngroup.com/wp-content/uploads/callottery_csr.png",
                        "large-width": 912,
                        "large-height": 532,
                        "large2x": "https://pagedesigngroup.com/wp-content/uploads/callottery_csr.png",
                        "large2x-width": 912,
                        "large2x-height": 532,
                        "hero": "https://pagedesigngroup.com/wp-content/uploads/callottery_csr.png",
                        "hero-width": 912,
                        "hero-height": 532,
                        "medium2x": "https://pagedesigngroup.com/wp-content/uploads/callottery_csr-440x257.png",
                        "medium2x-width": 440,
                        "medium2x-height": 257,
                        "story-tmb": "https://pagedesigngroup.com/wp-content/uploads/callottery_csr-300x300.png",
                        "story-tmb-width": 300,
                        "story-tmb-height": 300,
                        "hero-m": "https://pagedesigngroup.com/wp-content/uploads/callottery_csr-700x408.png",
                        "hero-m-width": 700,
                        "hero-m-height": 408
                    }
                }
            ],
            "file_download": false,
            "website_url": "",
            "video_truefalse": false
        },
        "_links": {
            "self": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api/1599"
                }
            ],
            "collection": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api"
                }
            ],
            "about": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/types/portfolio"
                }
            ],
            "version-history": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api/1599/revisions"
                }
            ],
            "wp:featuredmedia": [
                {
                    "embeddable": true,
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/media/1601"
                }
            ],
            "wp:attachment": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/media?parent=1599"
                }
            ],
            "wp:term": [
                {
                    "taxonomy": "post_tag",
                    "embeddable": true,
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags?post=1599"
                }
            ],
            "curies": [
                {
                    "name": "wp",
                    "href": "https://api.w.org/{rel}",
                    "templated": true
                }
            ]
        },
        "_embedded": {
            "wp:featuredmedia": [
                {
                    "id": 1601,
                    "date": "2017-03-20T19:58:37",
                    "slug": "callottery_csr-2",
                    "type": "attachment",
                    "link": "https://pagedesigngroup.com/portfolio/corporate-social-responsibility-report/attachment/callottery_csr-2/",
                    "title": {
                        "rendered": "callottery_csr"
                    },
                    "author": 1,
                    "acf": [],
                    "caption": {
                        "rendered": ""
                    },
                    "alt_text": "",
                    "media_type": "image",
                    "mime_type": "image/png",
                    "media_details": {
                        "width": 370,
                        "height": 379,
                        "file": "callottery_csr-1.png",
                        "sizes": {
                            "thumbnail": {
                                "file": "callottery_csr-1-150x150.png",
                                "width": 150,
                                "height": 150,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/callottery_csr-1-150x150.png"
                            },
                            "medium": {
                                "file": "callottery_csr-1-215x220.png",
                                "width": 215,
                                "height": 220,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/callottery_csr-1-215x220.png"
                            },
                            "story-tmb": {
                                "file": "callottery_csr-1-300x300.png",
                                "width": 300,
                                "height": 300,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/callottery_csr-1-300x300.png"
                            },
                            "full": {
                                "file": "callottery_csr-1.png",
                                "width": 370,
                                "height": 379,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/callottery_csr-1.png"
                            }
                        },
                        "image_meta": {
                            "aperture": "0",
                            "credit": "",
                            "camera": "",
                            "caption": "",
                            "created_timestamp": "0",
                            "copyright": "",
                            "focal_length": "0",
                            "iso": "0",
                            "shutter_speed": "0",
                            "title": "",
                            "orientation": "0",
                            "keywords": []
                        }
                    },
                    "source_url": "https://pagedesigngroup.com/wp-content/uploads/callottery_csr-1.png",
                    "_links": {
                        "self": [
                            {
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/media/1601"
                            }
                        ],
                        "collection": [
                            {
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/media"
                            }
                        ],
                        "about": [
                            {
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/types/attachment"
                            }
                        ],
                        "author": [
                            {
                                "embeddable": true,
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/users/1"
                            }
                        ],
                        "replies": [
                            {
                                "embeddable": true,
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/comments?post=1601"
                            }
                        ]
                    }
                }
            ]
        }
    },
    {
        "id": 1596,
        "date": "2017-03-17T19:43:15",
        "date_gmt": "2017-03-17T19:43:15",
        "guid": {
            "rendered": "http://pdg16.wpengine.com/?post_type=portfolio&#038;p=1596"
        },
        "modified": "2017-03-29T16:47:47",
        "modified_gmt": "2017-03-29T16:47:47",
        "slug": "sacramento-valley-video",
        "status": "publish",
        "type": "portfolio",
        "link": "https://pagedesigngroup.com/portfolio/sacramento-valley-video/",
        "title": {
            "rendered": "Sacramento Valley Video"
        },
        "content": {
            "rendered": "<p>A combination of still photography and video footage was used to describe the mission of the Northern California Water Association and its relation to the agriculture industry in California. We wrote the script, recorded the audio and performed all the editing to create this captivating video.</p>\n",
            "protected": false
        },
        "excerpt": {
            "rendered": "<p>A combination of still photography and video footage was used to describe the mission of the Northern California Water Association and its relation to the agriculture industry in California. We wrote the script, recorded the audio and performed all the editing to create this captivating video.</p>\n",
            "protected": false
        },
        "featured_media": 1594,
        "parent": 0,
        "template": "",
        "tags": [
            11,
            18,
            40,
            34,
            41
        ],
        "acf": {
            "client": "Northern California Water Association",
            "full_size_image": false,
            "file_download": false,
            "website_url": "",
            "video_truefalse": true,
            "video": [
                {
                    "video_title": "",
                    "video_url": "207512305"
                }
            ]
        },
        "_links": {
            "self": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api/1596"
                }
            ],
            "collection": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api"
                }
            ],
            "about": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/types/portfolio"
                }
            ],
            "version-history": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api/1596/revisions"
                }
            ],
            "wp:featuredmedia": [
                {
                    "embeddable": true,
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/media/1594"
                }
            ],
            "wp:attachment": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/media?parent=1596"
                }
            ],
            "wp:term": [
                {
                    "taxonomy": "post_tag",
                    "embeddable": true,
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags?post=1596"
                }
            ],
            "curies": [
                {
                    "name": "wp",
                    "href": "https://api.w.org/{rel}",
                    "templated": true
                }
            ]
        },
        "_embedded": {
            "wp:featuredmedia": [
                {
                    "id": 1594,
                    "date": "2017-03-17T19:38:32",
                    "slug": "ncwa_video",
                    "type": "attachment",
                    "link": "https://pagedesigngroup.com/ncwa_video/",
                    "title": {
                        "rendered": "ncwa_video"
                    },
                    "author": 1,
                    "acf": [],
                    "caption": {
                        "rendered": ""
                    },
                    "alt_text": "",
                    "media_type": "image",
                    "mime_type": "image/png",
                    "media_details": {
                        "width": 370,
                        "height": 251,
                        "file": "ncwa_video.png",
                        "sizes": {
                            "thumbnail": {
                                "file": "ncwa_video-150x150.png",
                                "width": 150,
                                "height": 150,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/ncwa_video-150x150.png"
                            },
                            "medium": {
                                "file": "ncwa_video-220x149.png",
                                "width": 220,
                                "height": 149,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/ncwa_video-220x149.png"
                            },
                            "story-tmb": {
                                "file": "ncwa_video-300x251.png",
                                "width": 300,
                                "height": 251,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/ncwa_video-300x251.png"
                            },
                            "full": {
                                "file": "ncwa_video.png",
                                "width": 370,
                                "height": 251,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/ncwa_video.png"
                            }
                        },
                        "image_meta": {
                            "aperture": "0",
                            "credit": "",
                            "camera": "",
                            "caption": "",
                            "created_timestamp": "0",
                            "copyright": "",
                            "focal_length": "0",
                            "iso": "0",
                            "shutter_speed": "0",
                            "title": "",
                            "orientation": "0",
                            "keywords": []
                        }
                    },
                    "source_url": "https://pagedesigngroup.com/wp-content/uploads/ncwa_video.png",
                    "_links": {
                        "self": [
                            {
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/media/1594"
                            }
                        ],
                        "collection": [
                            {
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/media"
                            }
                        ],
                        "about": [
                            {
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/types/attachment"
                            }
                        ],
                        "author": [
                            {
                                "embeddable": true,
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/users/1"
                            }
                        ],
                        "replies": [
                            {
                                "embeddable": true,
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/comments?post=1594"
                            }
                        ]
                    }
                }
            ],
            "wp:term": [
                [
                    {
                        "id": 11,
                        "link": "https://pagedesigngroup.com/tag/agriculture/",
                        "name": "Agriculture",
                        "slug": "agriculture",
                        "taxonomy": "post_tag",
                        "_links": {
                            "self": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags/11"
                                }
                            ],
                            "collection": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags"
                                }
                            ],
                            "about": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/taxonomies/post_tag"
                                }
                            ],
                            "wp:post_type": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/posts?tags=11"
                                },
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api?tags=11"
                                }
                            ],
                            "curies": [
                                {
                                    "name": "wp",
                                    "href": "https://api.w.org/{rel}",
                                    "templated": true
                                }
                            ]
                        }
                    },
                    {
                        "id": 18,
                        "link": "https://pagedesigngroup.com/tag/education/",
                        "name": "Education",
                        "slug": "education",
                        "taxonomy": "post_tag",
                        "_links": {
                            "self": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags/18"
                                }
                            ],
                            "collection": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags"
                                }
                            ],
                            "about": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/taxonomies/post_tag"
                                }
                            ],
                            "wp:post_type": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/posts?tags=18"
                                },
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api?tags=18"
                                }
                            ],
                            "curies": [
                                {
                                    "name": "wp",
                                    "href": "https://api.w.org/{rel}",
                                    "templated": true
                                }
                            ]
                        }
                    },
                    {
                        "id": 40,
                        "link": "https://pagedesigngroup.com/tag/environment/",
                        "name": "Environment",
                        "slug": "environment",
                        "taxonomy": "post_tag",
                        "_links": {
                            "self": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags/40"
                                }
                            ],
                            "collection": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags"
                                }
                            ],
                            "about": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/taxonomies/post_tag"
                                }
                            ],
                            "wp:post_type": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/posts?tags=40"
                                },
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api?tags=40"
                                }
                            ],
                            "curies": [
                                {
                                    "name": "wp",
                                    "href": "https://api.w.org/{rel}",
                                    "templated": true
                                }
                            ]
                        }
                    },
                    {
                        "id": 34,
                        "link": "https://pagedesigngroup.com/tag/water/",
                        "name": "Water",
                        "slug": "water",
                        "taxonomy": "post_tag",
                        "_links": {
                            "self": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags/34"
                                }
                            ],
                            "collection": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags"
                                }
                            ],
                            "about": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/taxonomies/post_tag"
                                }
                            ],
                            "wp:post_type": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/posts?tags=34"
                                },
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api?tags=34"
                                }
                            ],
                            "curies": [
                                {
                                    "name": "wp",
                                    "href": "https://api.w.org/{rel}",
                                    "templated": true
                                }
                            ]
                        }
                    },
                    {
                        "id": 41,
                        "link": "https://pagedesigngroup.com/tag/wildlife/",
                        "name": "Wildlife",
                        "slug": "wildlife",
                        "taxonomy": "post_tag",
                        "_links": {
                            "self": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags/41"
                                }
                            ],
                            "collection": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags"
                                }
                            ],
                            "about": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/taxonomies/post_tag"
                                }
                            ],
                            "wp:post_type": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/posts?tags=41"
                                },
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api?tags=41"
                                }
                            ],
                            "curies": [
                                {
                                    "name": "wp",
                                    "href": "https://api.w.org/{rel}",
                                    "templated": true
                                }
                            ]
                        }
                    }
                ]
            ]
        }
    },
    {
        "id": 1591,
        "date": "2017-03-17T19:07:38",
        "date_gmt": "2017-03-17T19:07:38",
        "guid": {
            "rendered": "http://pdg16.wpengine.com/?post_type=portfolio&#038;p=1591"
        },
        "modified": "2017-03-29T16:56:59",
        "modified_gmt": "2017-03-29T16:56:59",
        "slug": "fish-food-animation",
        "status": "publish",
        "type": "portfolio",
        "link": "https://pagedesigngroup.com/portfolio/fish-food-animation/",
        "title": {
            "rendered": "Fish Food Animation"
        },
        "content": {
            "rendered": "<p>With this stylish animation, the California Rice Commission educates people on how important flooded rice fields are to the Sacramento Valley wildlife population. This piece shows how flooded rice fields are now providing a perfect nursery for baby salmon, one of the more threatened species in the valley.</p>\n",
            "protected": false
        },
        "excerpt": {
            "rendered": "<p>With this stylish animation, the California Rice Commission educates people on how important flooded rice fields are to the Sacramento Valley wildlife population. This piece shows how flooded rice fields are now providing a perfect nursery for baby salmon, one of the more threatened species in the valley.</p>\n",
            "protected": false
        },
        "featured_media": 1592,
        "parent": 0,
        "template": "",
        "tags": [
            18,
            34,
            41
        ],
        "acf": {
            "client": "California Rice Commission",
            "full_size_image": false,
            "file_download": false,
            "website_url": "",
            "video_truefalse": true,
            "video": [
                {
                    "video_title": "Fish Food Animation ",
                    "video_url": "208753701"
                }
            ]
        },
        "_links": {
            "self": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api/1591"
                }
            ],
            "collection": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api"
                }
            ],
            "about": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/types/portfolio"
                }
            ],
            "version-history": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api/1591/revisions"
                }
            ],
            "wp:featuredmedia": [
                {
                    "embeddable": true,
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/media/1592"
                }
            ],
            "wp:attachment": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/media?parent=1591"
                }
            ],
            "wp:term": [
                {
                    "taxonomy": "post_tag",
                    "embeddable": true,
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags?post=1591"
                }
            ],
            "curies": [
                {
                    "name": "wp",
                    "href": "https://api.w.org/{rel}",
                    "templated": true
                }
            ]
        },
        "_embedded": {
            "wp:featuredmedia": [
                {
                    "id": 1592,
                    "date": "2017-03-17T19:07:29",
                    "slug": "fish_food",
                    "type": "attachment",
                    "link": "https://pagedesigngroup.com/portfolio/fish-food-animation/attachment/fish_food/",
                    "title": {
                        "rendered": "fish_food"
                    },
                    "author": 1,
                    "acf": [],
                    "caption": {
                        "rendered": ""
                    },
                    "alt_text": "",
                    "media_type": "image",
                    "mime_type": "image/png",
                    "media_details": {
                        "width": 370,
                        "height": 251,
                        "file": "fish_food.png",
                        "sizes": {
                            "thumbnail": {
                                "file": "fish_food-150x150.png",
                                "width": 150,
                                "height": 150,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/fish_food-150x150.png"
                            },
                            "medium": {
                                "file": "fish_food-220x149.png",
                                "width": 220,
                                "height": 149,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/fish_food-220x149.png"
                            },
                            "story-tmb": {
                                "file": "fish_food-300x251.png",
                                "width": 300,
                                "height": 251,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/fish_food-300x251.png"
                            },
                            "full": {
                                "file": "fish_food.png",
                                "width": 370,
                                "height": 251,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/fish_food.png"
                            }
                        },
                        "image_meta": {
                            "aperture": "0",
                            "credit": "",
                            "camera": "",
                            "caption": "",
                            "created_timestamp": "0",
                            "copyright": "",
                            "focal_length": "0",
                            "iso": "0",
                            "shutter_speed": "0",
                            "title": "",
                            "orientation": "0",
                            "keywords": []
                        }
                    },
                    "source_url": "https://pagedesigngroup.com/wp-content/uploads/fish_food.png",
                    "_links": {
                        "self": [
                            {
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/media/1592"
                            }
                        ],
                        "collection": [
                            {
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/media"
                            }
                        ],
                        "about": [
                            {
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/types/attachment"
                            }
                        ],
                        "author": [
                            {
                                "embeddable": true,
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/users/1"
                            }
                        ],
                        "replies": [
                            {
                                "embeddable": true,
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/comments?post=1592"
                            }
                        ]
                    }
                }
            ],
            "wp:term": [
                [
                    {
                        "id": 18,
                        "link": "https://pagedesigngroup.com/tag/education/",
                        "name": "Education",
                        "slug": "education",
                        "taxonomy": "post_tag",
                        "_links": {
                            "self": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags/18"
                                }
                            ],
                            "collection": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags"
                                }
                            ],
                            "about": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/taxonomies/post_tag"
                                }
                            ],
                            "wp:post_type": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/posts?tags=18"
                                },
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api?tags=18"
                                }
                            ],
                            "curies": [
                                {
                                    "name": "wp",
                                    "href": "https://api.w.org/{rel}",
                                    "templated": true
                                }
                            ]
                        }
                    },
                    {
                        "id": 34,
                        "link": "https://pagedesigngroup.com/tag/water/",
                        "name": "Water",
                        "slug": "water",
                        "taxonomy": "post_tag",
                        "_links": {
                            "self": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags/34"
                                }
                            ],
                            "collection": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags"
                                }
                            ],
                            "about": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/taxonomies/post_tag"
                                }
                            ],
                            "wp:post_type": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/posts?tags=34"
                                },
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api?tags=34"
                                }
                            ],
                            "curies": [
                                {
                                    "name": "wp",
                                    "href": "https://api.w.org/{rel}",
                                    "templated": true
                                }
                            ]
                        }
                    },
                    {
                        "id": 41,
                        "link": "https://pagedesigngroup.com/tag/wildlife/",
                        "name": "Wildlife",
                        "slug": "wildlife",
                        "taxonomy": "post_tag",
                        "_links": {
                            "self": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags/41"
                                }
                            ],
                            "collection": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags"
                                }
                            ],
                            "about": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/taxonomies/post_tag"
                                }
                            ],
                            "wp:post_type": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/posts?tags=41"
                                },
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api?tags=41"
                                }
                            ],
                            "curies": [
                                {
                                    "name": "wp",
                                    "href": "https://api.w.org/{rel}",
                                    "templated": true
                                }
                            ]
                        }
                    }
                ]
            ]
        }
    },
    {
        "id": 1587,
        "date": "2017-03-17T17:44:20",
        "date_gmt": "2017-03-17T17:44:20",
        "guid": {
            "rendered": "http://pdg16.wpengine.com/?post_type=portfolio&#038;p=1587"
        },
        "modified": "2017-03-31T18:38:18",
        "modified_gmt": "2017-03-31T18:38:18",
        "slug": "how-to-read-a-food-label-card",
        "status": "publish",
        "type": "portfolio",
        "link": "https://pagedesigngroup.com/portfolio/how-to-read-a-food-label-card/",
        "title": {
            "rendered": "How to Read a Food Label Card"
        },
        "content": {
            "rendered": "<p>To help extend the mission of UC Davis Integrative Medicine, we developed this handy and colorful cheat sheet to help shoppers interpret food labels and make better nutritional buying decisions at the grocery store. This laminated card was also perforated so that a smaller card could be detached and conveniently carried in a wallet.</p>\n",
            "protected": false
        },
        "excerpt": {
            "rendered": "<p>To help extend the mission of UC Davis Integrative Medicine, we developed this handy and colorful cheat sheet to help shoppers interpret food labels and make better nutritional buying decisions at the grocery store. This laminated card was also perforated so that a smaller card could be detached and conveniently carried in a wallet.</p>\n",
            "protected": false
        },
        "featured_media": 1588,
        "parent": 0,
        "template": "",
        "tags": [
            18,
            60
        ],
        "acf": {
            "client": "UC Davis Health, Department of Integrative Medicine",
            "full_size_image": [
                {
                    "ID": 1589,
                    "id": 1589,
                    "title": "UCDIM-How-to-read",
                    "filename": "UCDIM-How-to-read.png",
                    "url": "https://pagedesigngroup.com/wp-content/uploads/UCDIM-How-to-read.png",
                    "alt": "",
                    "author": "1",
                    "description": "",
                    "caption": "",
                    "name": "ucdim-how-to-read",
                    "date": "2017-03-17 17:43:35",
                    "modified": "2017-03-17 17:44:11",
                    "mime_type": "image/png",
                    "type": "image",
                    "icon": "https://pagedesigngroup.com/wp-includes/images/media/default.png",
                    "width": 909,
                    "height": 626,
                    "sizes": {
                        "thumbnail": "https://pagedesigngroup.com/wp-content/uploads/UCDIM-How-to-read-150x150.png",
                        "thumbnail-width": 150,
                        "thumbnail-height": 150,
                        "medium": "https://pagedesigngroup.com/wp-content/uploads/UCDIM-How-to-read-220x152.png",
                        "medium-width": 220,
                        "medium-height": 152,
                        "medium_large": "https://pagedesigngroup.com/wp-content/uploads/UCDIM-How-to-read-768x529.png",
                        "medium_large-width": 768,
                        "medium_large-height": 529,
                        "large": "https://pagedesigngroup.com/wp-content/uploads/UCDIM-How-to-read.png",
                        "large-width": 909,
                        "large-height": 626,
                        "large2x": "https://pagedesigngroup.com/wp-content/uploads/UCDIM-How-to-read.png",
                        "large2x-width": 909,
                        "large2x-height": 626,
                        "hero": "https://pagedesigngroup.com/wp-content/uploads/UCDIM-How-to-read.png",
                        "hero-width": 909,
                        "hero-height": 626,
                        "medium2x": "https://pagedesigngroup.com/wp-content/uploads/UCDIM-How-to-read-440x303.png",
                        "medium2x-width": 440,
                        "medium2x-height": 303,
                        "story-tmb": "https://pagedesigngroup.com/wp-content/uploads/UCDIM-How-to-read-300x300.png",
                        "story-tmb-width": 300,
                        "story-tmb-height": 300,
                        "hero-m": "https://pagedesigngroup.com/wp-content/uploads/UCDIM-How-to-read-700x482.png",
                        "hero-m-width": 700,
                        "hero-m-height": 482
                    }
                }
            ],
            "file_download": false,
            "website_url": "",
            "video_truefalse": false
        },
        "_links": {
            "self": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api/1587"
                }
            ],
            "collection": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api"
                }
            ],
            "about": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/types/portfolio"
                }
            ],
            "version-history": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api/1587/revisions"
                }
            ],
            "wp:featuredmedia": [
                {
                    "embeddable": true,
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/media/1588"
                }
            ],
            "wp:attachment": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/media?parent=1587"
                }
            ],
            "wp:term": [
                {
                    "taxonomy": "post_tag",
                    "embeddable": true,
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags?post=1587"
                }
            ],
            "curies": [
                {
                    "name": "wp",
                    "href": "https://api.w.org/{rel}",
                    "templated": true
                }
            ]
        },
        "_embedded": {
            "wp:featuredmedia": [
                {
                    "id": 1588,
                    "date": "2017-03-17T17:43:09",
                    "slug": "ucdim-how-to-read-baby-2",
                    "type": "attachment",
                    "link": "https://pagedesigngroup.com/portfolio/how-to-read-a-food-label-card/attachment/ucdim-how-to-read-baby-2/",
                    "title": {
                        "rendered": "UCDIM-How-to-read-baby"
                    },
                    "author": 1,
                    "acf": [],
                    "caption": {
                        "rendered": ""
                    },
                    "alt_text": "",
                    "media_type": "image",
                    "mime_type": "image/png",
                    "media_details": {
                        "width": 370,
                        "height": 379,
                        "file": "UCDIM-How-to-read-baby-1.png",
                        "sizes": {
                            "thumbnail": {
                                "file": "UCDIM-How-to-read-baby-1-150x150.png",
                                "width": 150,
                                "height": 150,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/UCDIM-How-to-read-baby-1-150x150.png"
                            },
                            "medium": {
                                "file": "UCDIM-How-to-read-baby-1-215x220.png",
                                "width": 215,
                                "height": 220,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/UCDIM-How-to-read-baby-1-215x220.png"
                            },
                            "story-tmb": {
                                "file": "UCDIM-How-to-read-baby-1-300x300.png",
                                "width": 300,
                                "height": 300,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/UCDIM-How-to-read-baby-1-300x300.png"
                            },
                            "full": {
                                "file": "UCDIM-How-to-read-baby-1.png",
                                "width": 370,
                                "height": 379,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/UCDIM-How-to-read-baby-1.png"
                            }
                        },
                        "image_meta": {
                            "aperture": "0",
                            "credit": "",
                            "camera": "",
                            "caption": "",
                            "created_timestamp": "0",
                            "copyright": "",
                            "focal_length": "0",
                            "iso": "0",
                            "shutter_speed": "0",
                            "title": "",
                            "orientation": "0",
                            "keywords": []
                        }
                    },
                    "source_url": "https://pagedesigngroup.com/wp-content/uploads/UCDIM-How-to-read-baby-1.png",
                    "_links": {
                        "self": [
                            {
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/media/1588"
                            }
                        ],
                        "collection": [
                            {
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/media"
                            }
                        ],
                        "about": [
                            {
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/types/attachment"
                            }
                        ],
                        "author": [
                            {
                                "embeddable": true,
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/users/1"
                            }
                        ],
                        "replies": [
                            {
                                "embeddable": true,
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/comments?post=1588"
                            }
                        ]
                    }
                }
            ],
            "wp:term": [
                [
                    {
                        "id": 18,
                        "link": "https://pagedesigngroup.com/tag/education/",
                        "name": "Education",
                        "slug": "education",
                        "taxonomy": "post_tag",
                        "_links": {
                            "self": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags/18"
                                }
                            ],
                            "collection": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags"
                                }
                            ],
                            "about": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/taxonomies/post_tag"
                                }
                            ],
                            "wp:post_type": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/posts?tags=18"
                                },
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api?tags=18"
                                }
                            ],
                            "curies": [
                                {
                                    "name": "wp",
                                    "href": "https://api.w.org/{rel}",
                                    "templated": true
                                }
                            ]
                        }
                    },
                    {
                        "id": 60,
                        "link": "https://pagedesigngroup.com/tag/food/",
                        "name": "Food",
                        "slug": "food",
                        "taxonomy": "post_tag",
                        "_links": {
                            "self": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags/60"
                                }
                            ],
                            "collection": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags"
                                }
                            ],
                            "about": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/taxonomies/post_tag"
                                }
                            ],
                            "wp:post_type": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/posts?tags=60"
                                },
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api?tags=60"
                                }
                            ],
                            "curies": [
                                {
                                    "name": "wp",
                                    "href": "https://api.w.org/{rel}",
                                    "templated": true
                                }
                            ]
                        }
                    }
                ]
            ]
        }
    },
    {
        "id": 1581,
        "date": "2017-03-16T22:27:50",
        "date_gmt": "2017-03-16T22:27:50",
        "guid": {
            "rendered": "http://pdg16.wpengine.com/?post_type=portfolio&#038;p=1581"
        },
        "modified": "2017-03-31T19:45:49",
        "modified_gmt": "2017-03-31T19:45:49",
        "slug": "1581-2",
        "status": "publish",
        "type": "portfolio",
        "link": "https://pagedesigngroup.com/portfolio/1581-2/",
        "title": {
            "rendered": "Nursing Magazine"
        },
        "content": {
            "rendered": "<p>The UC Davis Betty Irene Moore School of Nursing asked us to redesign their website to a more user-friendly format. They are the second most visited website in the UC Davis Health. Once we completed that project successfully, we began working on their brand new annual magazine highlighting the amazing work done at the school.</p>\n",
            "protected": false
        },
        "excerpt": {
            "rendered": "<p>The UC Davis Betty Irene Moore School of Nursing asked us to redesign their website to a more user-friendly format. They are the second most visited website in the UC Davis Health. Once we completed that project successfully, we began working on their brand new annual magazine highlighting the amazing work done at the school.</p>\n",
            "protected": false
        },
        "featured_media": 1585,
        "parent": 0,
        "template": "",
        "tags": [
            18,
            53,
            123
        ],
        "acf": {
            "client": "UC Davis Betty Irene Moore School of Nursing",
            "full_size_image": [
                {
                    "ID": 1582,
                    "id": 1582,
                    "title": "bimson",
                    "filename": "bimson.png",
                    "url": "https://pagedesigngroup.com/wp-content/uploads/bimson.png",
                    "alt": "",
                    "author": "1",
                    "description": "",
                    "caption": "",
                    "name": "bimson",
                    "date": "2017-03-16 22:27:39",
                    "modified": "2017-03-16 22:44:18",
                    "mime_type": "image/png",
                    "type": "image",
                    "icon": "https://pagedesigngroup.com/wp-includes/images/media/default.png",
                    "width": 892,
                    "height": 603,
                    "sizes": {
                        "thumbnail": "https://pagedesigngroup.com/wp-content/uploads/bimson-150x150.png",
                        "thumbnail-width": 150,
                        "thumbnail-height": 150,
                        "medium": "https://pagedesigngroup.com/wp-content/uploads/bimson-220x149.png",
                        "medium-width": 220,
                        "medium-height": 149,
                        "medium_large": "https://pagedesigngroup.com/wp-content/uploads/bimson-768x519.png",
                        "medium_large-width": 768,
                        "medium_large-height": 519,
                        "large": "https://pagedesigngroup.com/wp-content/uploads/bimson.png",
                        "large-width": 892,
                        "large-height": 603,
                        "large2x": "https://pagedesigngroup.com/wp-content/uploads/bimson.png",
                        "large2x-width": 892,
                        "large2x-height": 603,
                        "hero": "https://pagedesigngroup.com/wp-content/uploads/bimson.png",
                        "hero-width": 892,
                        "hero-height": 603,
                        "medium2x": "https://pagedesigngroup.com/wp-content/uploads/bimson-440x297.png",
                        "medium2x-width": 440,
                        "medium2x-height": 297,
                        "story-tmb": "https://pagedesigngroup.com/wp-content/uploads/bimson-300x300.png",
                        "story-tmb-width": 300,
                        "story-tmb-height": 300,
                        "hero-m": "https://pagedesigngroup.com/wp-content/uploads/bimson-700x473.png",
                        "hero-m-width": 700,
                        "hero-m-height": 473
                    }
                },
                {
                    "ID": 1586,
                    "id": 1586,
                    "title": "bimson_online_magazine",
                    "filename": "bimson_online_magazine.png",
                    "url": "https://pagedesigngroup.com/wp-content/uploads/bimson_online_magazine.png",
                    "alt": "",
                    "author": "1",
                    "description": "",
                    "caption": "",
                    "name": "bimson_online_magazine",
                    "date": "2017-03-17 17:21:20",
                    "modified": "2017-03-17 17:21:27",
                    "mime_type": "image/png",
                    "type": "image",
                    "icon": "https://pagedesigngroup.com/wp-includes/images/media/default.png",
                    "width": 977,
                    "height": 546,
                    "sizes": {
                        "thumbnail": "https://pagedesigngroup.com/wp-content/uploads/bimson_online_magazine-150x150.png",
                        "thumbnail-width": 150,
                        "thumbnail-height": 150,
                        "medium": "https://pagedesigngroup.com/wp-content/uploads/bimson_online_magazine-220x123.png",
                        "medium-width": 220,
                        "medium-height": 123,
                        "medium_large": "https://pagedesigngroup.com/wp-content/uploads/bimson_online_magazine-768x429.png",
                        "medium_large-width": 768,
                        "medium_large-height": 429,
                        "large": "https://pagedesigngroup.com/wp-content/uploads/bimson_online_magazine.png",
                        "large-width": 977,
                        "large-height": 546,
                        "large2x": "https://pagedesigngroup.com/wp-content/uploads/bimson_online_magazine.png",
                        "large2x-width": 977,
                        "large2x-height": 546,
                        "hero": "https://pagedesigngroup.com/wp-content/uploads/bimson_online_magazine.png",
                        "hero-width": 977,
                        "hero-height": 546,
                        "medium2x": "https://pagedesigngroup.com/wp-content/uploads/bimson_online_magazine-440x246.png",
                        "medium2x-width": 440,
                        "medium2x-height": 246,
                        "story-tmb": "https://pagedesigngroup.com/wp-content/uploads/bimson_online_magazine-300x300.png",
                        "story-tmb-width": 300,
                        "story-tmb-height": 300,
                        "hero-m": "https://pagedesigngroup.com/wp-content/uploads/bimson_online_magazine-700x391.png",
                        "hero-m-width": 700,
                        "hero-m-height": 391
                    }
                }
            ],
            "file_download": false,
            "website_url": "https://www.ucdmc.ucdavis.edu/nursingmagazine/index.html",
            "video_truefalse": false
        },
        "_links": {
            "self": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api/1581"
                }
            ],
            "collection": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api"
                }
            ],
            "about": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/types/portfolio"
                }
            ],
            "version-history": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api/1581/revisions"
                }
            ],
            "wp:featuredmedia": [
                {
                    "embeddable": true,
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/media/1585"
                }
            ],
            "wp:attachment": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/media?parent=1581"
                }
            ],
            "wp:term": [
                {
                    "taxonomy": "post_tag",
                    "embeddable": true,
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags?post=1581"
                }
            ],
            "curies": [
                {
                    "name": "wp",
                    "href": "https://api.w.org/{rel}",
                    "templated": true
                }
            ]
        },
        "_embedded": {
            "wp:featuredmedia": [
                {
                    "id": 1585,
                    "date": "2017-03-17T17:05:04",
                    "slug": "bimson_mag",
                    "type": "attachment",
                    "link": "https://pagedesigngroup.com/portfolio/1581-2/attachment/bimson_mag/",
                    "title": {
                        "rendered": "bimson_mag"
                    },
                    "author": 1,
                    "acf": [],
                    "caption": {
                        "rendered": ""
                    },
                    "alt_text": "",
                    "media_type": "image",
                    "mime_type": "image/png",
                    "media_details": {
                        "width": 370,
                        "height": 379,
                        "file": "bimson_mag.png",
                        "sizes": {
                            "thumbnail": {
                                "file": "bimson_mag-150x150.png",
                                "width": 150,
                                "height": 150,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/bimson_mag-150x150.png"
                            },
                            "medium": {
                                "file": "bimson_mag-215x220.png",
                                "width": 215,
                                "height": 220,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/bimson_mag-215x220.png"
                            },
                            "story-tmb": {
                                "file": "bimson_mag-300x300.png",
                                "width": 300,
                                "height": 300,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/bimson_mag-300x300.png"
                            },
                            "full": {
                                "file": "bimson_mag.png",
                                "width": 370,
                                "height": 379,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/bimson_mag.png"
                            }
                        },
                        "image_meta": {
                            "aperture": "0",
                            "credit": "",
                            "camera": "",
                            "caption": "",
                            "created_timestamp": "0",
                            "copyright": "",
                            "focal_length": "0",
                            "iso": "0",
                            "shutter_speed": "0",
                            "title": "",
                            "orientation": "0",
                            "keywords": []
                        }
                    },
                    "source_url": "https://pagedesigngroup.com/wp-content/uploads/bimson_mag.png",
                    "_links": {
                        "self": [
                            {
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/media/1585"
                            }
                        ],
                        "collection": [
                            {
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/media"
                            }
                        ],
                        "about": [
                            {
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/types/attachment"
                            }
                        ],
                        "author": [
                            {
                                "embeddable": true,
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/users/1"
                            }
                        ],
                        "replies": [
                            {
                                "embeddable": true,
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/comments?post=1585"
                            }
                        ]
                    }
                }
            ],
            "wp:term": [
                [
                    {
                        "id": 18,
                        "link": "https://pagedesigngroup.com/tag/education/",
                        "name": "Education",
                        "slug": "education",
                        "taxonomy": "post_tag",
                        "_links": {
                            "self": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags/18"
                                }
                            ],
                            "collection": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags"
                                }
                            ],
                            "about": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/taxonomies/post_tag"
                                }
                            ],
                            "wp:post_type": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/posts?tags=18"
                                },
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api?tags=18"
                                }
                            ],
                            "curies": [
                                {
                                    "name": "wp",
                                    "href": "https://api.w.org/{rel}",
                                    "templated": true
                                }
                            ]
                        }
                    },
                    {
                        "id": 53,
                        "link": "https://pagedesigngroup.com/tag/magazine/",
                        "name": "Magazine",
                        "slug": "magazine",
                        "taxonomy": "post_tag",
                        "_links": {
                            "self": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags/53"
                                }
                            ],
                            "collection": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags"
                                }
                            ],
                            "about": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/taxonomies/post_tag"
                                }
                            ],
                            "wp:post_type": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/posts?tags=53"
                                },
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api?tags=53"
                                }
                            ],
                            "curies": [
                                {
                                    "name": "wp",
                                    "href": "https://api.w.org/{rel}",
                                    "templated": true
                                }
                            ]
                        }
                    },
                    {
                        "id": 123,
                        "link": "https://pagedesigngroup.com/tag/nursing/",
                        "name": "Nursing",
                        "slug": "nursing",
                        "taxonomy": "post_tag",
                        "_links": {
                            "self": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags/123"
                                }
                            ],
                            "collection": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags"
                                }
                            ],
                            "about": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/taxonomies/post_tag"
                                }
                            ],
                            "wp:post_type": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/posts?tags=123"
                                },
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api?tags=123"
                                }
                            ],
                            "curies": [
                                {
                                    "name": "wp",
                                    "href": "https://api.w.org/{rel}",
                                    "templated": true
                                }
                            ]
                        }
                    }
                ]
            ]
        }
    },
    {
        "id": 1572,
        "date": "2017-03-16T19:42:32",
        "date_gmt": "2017-03-16T19:42:32",
        "guid": {
            "rendered": "http://pdg16.wpengine.com/?post_type=portfolio&#038;p=1572"
        },
        "modified": "2017-03-30T19:46:30",
        "modified_gmt": "2017-03-30T19:46:30",
        "slug": "institute-of-innovation-brochure",
        "status": "publish",
        "type": "portfolio",
        "link": "https://pagedesigngroup.com/portfolio/institute-of-innovation-brochure/",
        "title": {
            "rendered": "10th Anniversary  Brochure"
        },
        "content": {
            "rendered": "<p><em>A Decade of Innovation </em>was created to highlight the many milestones of the Institute over the last ten years. The publication also acknowledges donors and supporters, and emphasizes the variety of ways in which the Institute fosters and advances innovation around the world.</p>\n",
            "protected": false
        },
        "excerpt": {
            "rendered": "<p>A Decade of Innovation was created to highlight the many milestones of the Institute over the last ten years. The publication also acknowledges donors and supporters, and emphasizes the variety of ways in which the Institute fosters and advances innovation around the world.</p>\n",
            "protected": false
        },
        "featured_media": 1574,
        "parent": 0,
        "template": "",
        "tags": [
            89,
            18
        ],
        "acf": {
            "client": "UC Davis Institute for Innovation and Entrepreneurship",
            "full_size_image": [
                {
                    "ID": 1573,
                    "id": 1573,
                    "title": "UCD-Innovation",
                    "filename": "UCD-Innovation.png",
                    "url": "https://pagedesigngroup.com/wp-content/uploads/UCD-Innovation.png",
                    "alt": "",
                    "author": "1",
                    "description": "",
                    "caption": "",
                    "name": "ucd-innovation",
                    "date": "2017-03-16 19:38:32",
                    "modified": "2017-03-16 19:38:55",
                    "mime_type": "image/png",
                    "type": "image",
                    "icon": "https://pagedesigngroup.com/wp-includes/images/media/default.png",
                    "width": 905,
                    "height": 537,
                    "sizes": {
                        "thumbnail": "https://pagedesigngroup.com/wp-content/uploads/UCD-Innovation-150x150.png",
                        "thumbnail-width": 150,
                        "thumbnail-height": 150,
                        "medium": "https://pagedesigngroup.com/wp-content/uploads/UCD-Innovation-220x131.png",
                        "medium-width": 220,
                        "medium-height": 131,
                        "medium_large": "https://pagedesigngroup.com/wp-content/uploads/UCD-Innovation-768x456.png",
                        "medium_large-width": 768,
                        "medium_large-height": 456,
                        "large": "https://pagedesigngroup.com/wp-content/uploads/UCD-Innovation.png",
                        "large-width": 905,
                        "large-height": 537,
                        "large2x": "https://pagedesigngroup.com/wp-content/uploads/UCD-Innovation.png",
                        "large2x-width": 905,
                        "large2x-height": 537,
                        "hero": "https://pagedesigngroup.com/wp-content/uploads/UCD-Innovation.png",
                        "hero-width": 905,
                        "hero-height": 537,
                        "medium2x": "https://pagedesigngroup.com/wp-content/uploads/UCD-Innovation-440x261.png",
                        "medium2x-width": 440,
                        "medium2x-height": 261,
                        "story-tmb": "https://pagedesigngroup.com/wp-content/uploads/UCD-Innovation-300x300.png",
                        "story-tmb-width": 300,
                        "story-tmb-height": 300,
                        "hero-m": "https://pagedesigngroup.com/wp-content/uploads/UCD-Innovation-700x415.png",
                        "hero-m-width": 700,
                        "hero-m-height": 415
                    }
                }
            ],
            "file_download": false,
            "website_url": "",
            "video_truefalse": false
        },
        "_links": {
            "self": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api/1572"
                }
            ],
            "collection": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api"
                }
            ],
            "about": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/types/portfolio"
                }
            ],
            "version-history": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api/1572/revisions"
                }
            ],
            "wp:featuredmedia": [
                {
                    "embeddable": true,
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/media/1574"
                }
            ],
            "wp:attachment": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/media?parent=1572"
                }
            ],
            "wp:term": [
                {
                    "taxonomy": "post_tag",
                    "embeddable": true,
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags?post=1572"
                }
            ],
            "curies": [
                {
                    "name": "wp",
                    "href": "https://api.w.org/{rel}",
                    "templated": true
                }
            ]
        },
        "_embedded": {
            "wp:featuredmedia": [
                {
                    "id": 1574,
                    "date": "2017-03-16T19:42:06",
                    "slug": "ucd-innovation-2",
                    "type": "attachment",
                    "link": "https://pagedesigngroup.com/portfolio/institute-of-innovation-brochure/attachment/ucd-innovation-2/",
                    "title": {
                        "rendered": "UCD-Innovation"
                    },
                    "author": 1,
                    "acf": [],
                    "caption": {
                        "rendered": ""
                    },
                    "alt_text": "",
                    "media_type": "image",
                    "mime_type": "image/png",
                    "media_details": {
                        "width": 370,
                        "height": 379,
                        "file": "UCD-Innovation-1.png",
                        "sizes": {
                            "thumbnail": {
                                "file": "UCD-Innovation-1-150x150.png",
                                "width": 150,
                                "height": 150,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/UCD-Innovation-1-150x150.png"
                            },
                            "medium": {
                                "file": "UCD-Innovation-1-215x220.png",
                                "width": 215,
                                "height": 220,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/UCD-Innovation-1-215x220.png"
                            },
                            "story-tmb": {
                                "file": "UCD-Innovation-1-300x300.png",
                                "width": 300,
                                "height": 300,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/UCD-Innovation-1-300x300.png"
                            },
                            "full": {
                                "file": "UCD-Innovation-1.png",
                                "width": 370,
                                "height": 379,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/UCD-Innovation-1.png"
                            }
                        },
                        "image_meta": {
                            "aperture": "0",
                            "credit": "",
                            "camera": "",
                            "caption": "",
                            "created_timestamp": "0",
                            "copyright": "",
                            "focal_length": "0",
                            "iso": "0",
                            "shutter_speed": "0",
                            "title": "",
                            "orientation": "0",
                            "keywords": []
                        }
                    },
                    "source_url": "https://pagedesigngroup.com/wp-content/uploads/UCD-Innovation-1.png",
                    "_links": {
                        "self": [
                            {
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/media/1574"
                            }
                        ],
                        "collection": [
                            {
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/media"
                            }
                        ],
                        "about": [
                            {
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/types/attachment"
                            }
                        ],
                        "author": [
                            {
                                "embeddable": true,
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/users/1"
                            }
                        ],
                        "replies": [
                            {
                                "embeddable": true,
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/comments?post=1574"
                            }
                        ]
                    }
                }
            ],
            "wp:term": [
                [
                    {
                        "id": 89,
                        "link": "https://pagedesigngroup.com/tag/brochure/",
                        "name": "Brochure",
                        "slug": "brochure",
                        "taxonomy": "post_tag",
                        "_links": {
                            "self": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags/89"
                                }
                            ],
                            "collection": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags"
                                }
                            ],
                            "about": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/taxonomies/post_tag"
                                }
                            ],
                            "wp:post_type": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/posts?tags=89"
                                },
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api?tags=89"
                                }
                            ],
                            "curies": [
                                {
                                    "name": "wp",
                                    "href": "https://api.w.org/{rel}",
                                    "templated": true
                                }
                            ]
                        }
                    },
                    {
                        "id": 18,
                        "link": "https://pagedesigngroup.com/tag/education/",
                        "name": "Education",
                        "slug": "education",
                        "taxonomy": "post_tag",
                        "_links": {
                            "self": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags/18"
                                }
                            ],
                            "collection": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags"
                                }
                            ],
                            "about": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/taxonomies/post_tag"
                                }
                            ],
                            "wp:post_type": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/posts?tags=18"
                                },
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api?tags=18"
                                }
                            ],
                            "curies": [
                                {
                                    "name": "wp",
                                    "href": "https://api.w.org/{rel}",
                                    "templated": true
                                }
                            ]
                        }
                    }
                ]
            ]
        }
    },
    {
        "id": 1568,
        "date": "2017-03-16T18:53:55",
        "date_gmt": "2017-03-16T18:53:55",
        "guid": {
            "rendered": "http://pdg16.wpengine.com/?post_type=portfolio&#038;p=1568"
        },
        "modified": "2017-03-24T18:46:42",
        "modified_gmt": "2017-03-24T18:46:42",
        "slug": "healthcare-website",
        "status": "publish",
        "type": "portfolio",
        "link": "https://pagedesigngroup.com/portfolio/healthcare-website/",
        "title": {
            "rendered": "Healthcare Advocacy Website"
        },
        "content": {
            "rendered": "<p>We designed and built the website for the County Health Executives Association of California (CHEAC). The website is a WordPress powered site featuring a job board and an automated weekly email digest system for updating their membership of news that happens each week.</p>\n",
            "protected": false
        },
        "excerpt": {
            "rendered": "<p>We designed and built the website for the County Health Executives Association of California (CHEAC). The website is a WordPress powered site featuring a job board and an automated weekly email digest system for updating their membership of news that happens each week.</p>\n",
            "protected": false
        },
        "featured_media": 1571,
        "parent": 0,
        "template": "",
        "tags": [
            16,
            26
        ],
        "acf": {
            "client": "CHEAC",
            "full_size_image": [
                {
                    "ID": 1570,
                    "id": 1570,
                    "title": "cheac",
                    "filename": "cheac.png",
                    "url": "https://pagedesigngroup.com/wp-content/uploads/cheac.png",
                    "alt": "",
                    "author": "1",
                    "description": "",
                    "caption": "",
                    "name": "cheac",
                    "date": "2017-03-16 18:53:42",
                    "modified": "2017-03-20 18:08:25",
                    "mime_type": "image/png",
                    "type": "image",
                    "icon": "https://pagedesigngroup.com/wp-includes/images/media/default.png",
                    "width": 980,
                    "height": 549,
                    "sizes": {
                        "thumbnail": "https://pagedesigngroup.com/wp-content/uploads/cheac-150x150.png",
                        "thumbnail-width": 150,
                        "thumbnail-height": 150,
                        "medium": "https://pagedesigngroup.com/wp-content/uploads/cheac-220x123.png",
                        "medium-width": 220,
                        "medium-height": 123,
                        "medium_large": "https://pagedesigngroup.com/wp-content/uploads/cheac-768x430.png",
                        "medium_large-width": 768,
                        "medium_large-height": 430,
                        "large": "https://pagedesigngroup.com/wp-content/uploads/cheac.png",
                        "large-width": 980,
                        "large-height": 549,
                        "large2x": "https://pagedesigngroup.com/wp-content/uploads/cheac.png",
                        "large2x-width": 980,
                        "large2x-height": 549,
                        "hero": "https://pagedesigngroup.com/wp-content/uploads/cheac.png",
                        "hero-width": 980,
                        "hero-height": 549,
                        "medium2x": "https://pagedesigngroup.com/wp-content/uploads/cheac-440x246.png",
                        "medium2x-width": 440,
                        "medium2x-height": 246,
                        "story-tmb": "https://pagedesigngroup.com/wp-content/uploads/cheac-300x300.png",
                        "story-tmb-width": 300,
                        "story-tmb-height": 300,
                        "hero-m": "https://pagedesigngroup.com/wp-content/uploads/cheac-700x392.png",
                        "hero-m-width": 700,
                        "hero-m-height": 392
                    }
                }
            ],
            "file_download": false,
            "website_url": "http://cheac.org",
            "video_truefalse": false
        },
        "_links": {
            "self": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api/1568"
                }
            ],
            "collection": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api"
                }
            ],
            "about": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/types/portfolio"
                }
            ],
            "version-history": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api/1568/revisions"
                }
            ],
            "wp:featuredmedia": [
                {
                    "embeddable": true,
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/media/1571"
                }
            ],
            "wp:attachment": [
                {
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/media?parent=1568"
                }
            ],
            "wp:term": [
                {
                    "taxonomy": "post_tag",
                    "embeddable": true,
                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags?post=1568"
                }
            ],
            "curies": [
                {
                    "name": "wp",
                    "href": "https://api.w.org/{rel}",
                    "templated": true
                }
            ]
        },
        "_embedded": {
            "wp:featuredmedia": [
                {
                    "id": 1571,
                    "date": "2017-03-16T19:24:22",
                    "slug": "cheac-2",
                    "type": "attachment",
                    "link": "https://pagedesigngroup.com/portfolio/healthcare-website/attachment/cheac-2/",
                    "title": {
                        "rendered": "cheac"
                    },
                    "author": 1,
                    "acf": [],
                    "caption": {
                        "rendered": ""
                    },
                    "alt_text": "",
                    "media_type": "image",
                    "mime_type": "image/png",
                    "media_details": {
                        "width": 370,
                        "height": 251,
                        "file": "cheac-1.png",
                        "sizes": {
                            "thumbnail": {
                                "file": "cheac-1-150x150.png",
                                "width": 150,
                                "height": 150,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/cheac-1-150x150.png"
                            },
                            "medium": {
                                "file": "cheac-1-220x149.png",
                                "width": 220,
                                "height": 149,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/cheac-1-220x149.png"
                            },
                            "story-tmb": {
                                "file": "cheac-1-300x251.png",
                                "width": 300,
                                "height": 251,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/cheac-1-300x251.png"
                            },
                            "full": {
                                "file": "cheac-1.png",
                                "width": 370,
                                "height": 251,
                                "mime_type": "image/png",
                                "source_url": "https://pagedesigngroup.com/wp-content/uploads/cheac-1.png"
                            }
                        },
                        "image_meta": {
                            "aperture": "0",
                            "credit": "",
                            "camera": "",
                            "caption": "",
                            "created_timestamp": "0",
                            "copyright": "",
                            "focal_length": "0",
                            "iso": "0",
                            "shutter_speed": "0",
                            "title": "",
                            "orientation": "0",
                            "keywords": []
                        }
                    },
                    "source_url": "https://pagedesigngroup.com/wp-content/uploads/cheac-1.png",
                    "_links": {
                        "self": [
                            {
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/media/1571"
                            }
                        ],
                        "collection": [
                            {
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/media"
                            }
                        ],
                        "about": [
                            {
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/types/attachment"
                            }
                        ],
                        "author": [
                            {
                                "embeddable": true,
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/users/1"
                            }
                        ],
                        "replies": [
                            {
                                "embeddable": true,
                                "href": "https://pagedesigngroup.com/wp-json/wp/v2/comments?post=1571"
                            }
                        ]
                    }
                }
            ],
            "wp:term": [
                [
                    {
                        "id": 16,
                        "link": "https://pagedesigngroup.com/tag/healthcare/",
                        "name": "Healthcare",
                        "slug": "healthcare",
                        "taxonomy": "post_tag",
                        "_links": {
                            "self": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags/16"
                                }
                            ],
                            "collection": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags"
                                }
                            ],
                            "about": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/taxonomies/post_tag"
                                }
                            ],
                            "wp:post_type": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/posts?tags=16"
                                },
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api?tags=16"
                                }
                            ],
                            "curies": [
                                {
                                    "name": "wp",
                                    "href": "https://api.w.org/{rel}",
                                    "templated": true
                                }
                            ]
                        }
                    },
                    {
                        "id": 26,
                        "link": "https://pagedesigngroup.com/tag/wordpress/",
                        "name": "WordPress",
                        "slug": "wordpress",
                        "taxonomy": "post_tag",
                        "_links": {
                            "self": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags/26"
                                }
                            ],
                            "collection": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/tags"
                                }
                            ],
                            "about": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/taxonomies/post_tag"
                                }
                            ],
                            "wp:post_type": [
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/posts?tags=26"
                                },
                                {
                                    "href": "https://pagedesigngroup.com/wp-json/wp/v2/portfolio-api?tags=26"
                                }
                            ],
                            "curies": [
                                {
                                    "name": "wp",
                                    "href": "https://api.w.org/{rel}",
                                    "templated": true
                                }
                            ]
                        }
                    }
                ]
            ]
        }
    }
]

export default posts