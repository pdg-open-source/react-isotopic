import React from 'react';


const Header = (props) => {
	return(
		<section className="page-intro">
			<h1>
				{props.headline}
			</h1>
			<h2>{props.tagline}</h2>
		</section>
	)
}

Header.PropTypes = {
	headline: React.PropTypes.string.isRequired,
	tagline: React.PropTypes.string.isRequired
}


export default Header;