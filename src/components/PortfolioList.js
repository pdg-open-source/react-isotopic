import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import PortfolioItem from './PortfolioItem';

class PortfolioList extends React.Component {


    render(){
        return (
            <div id="portfolio">
                <div className="container">
                    <ul className="portfolio-list grid effect-1" id="grid" data-isotope='{ "itemSelector": ".grid-item", "layoutMode": "fitRows" }'>
                        {/* list components */}
                        <ReactCSSTransitionGroup transitionName="example" transitionEnterTimeout={700} transitionLeaveTimeout={700}>
                            { this.props.posts.map( post => (
                                <PortfolioItem
                                    key={post.id}
                                    id={post.id}
                                    link={post.link}
                                    title={ post.title.rendered }
                                    client={post.acf.client}
                                    thumbnail={post._embedded['wp:featuredmedia'][0]['source_url']}
                                />
                            ) ) }
                        </ReactCSSTransitionGroup>

                    </ul>
                </div>
            </div>
        )
    }
}

export default PortfolioList