import React from 'react';

class PortfolioItem extends React.Component {

    render(){
        console.log(this.props.thumbnail)
        return(
            <li
                className="portfolio-item"
            >
                <a
                    href={ this.props.link }
                    className="img-wrap"
                >
                    <img
                        src={this.props.thumbnail}
                        alt={`${this.props.client} | ${this.props.title}`}
                        title=""
                    />

                    <div
                        className="description description-grid">
                        <div
                            className="details"
                        >
                            <h2>{ this.props.client }</h2>
                            <p>{ this.props.title }</p>
                        </div>
                    </div>
                </a>
            </li>
        )

    }
}

export default PortfolioItem;