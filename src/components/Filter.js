import React from 'react';
import FilterButton from './FilterButton';

class Filter extends React.Component {
	render() {
		const cats = ['*', 'education', 'brochure', 'web-design--development', 'packaging', 'multimedia', 'environmental'];
		return (
			<div className="portfolio-filter">
                <div className="container">
                    <span className="label">SHOW PROJECTS: </span>
                    <div className="filter-button-group">
                    	{cats.map((cat, index) => {
                    		return (<FilterButton catFilter={this.props.handleFilter} key={index} title={cat} filterData={cat} />
                    		)
                    	})}
            		</div>
                </div>
            </div>
		)
	}
}

export default Filter;