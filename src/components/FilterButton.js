import React from 'react';

class FilterButton extends React.Component {
	applyFilter = (e) => {
		e.preventDefault();
		const newFilter = this.props.title;
		this.props.catFilter(newFilter);
	}

	render() {
		return (
			<button onClick={(e) => this.applyFilter(e)} dataFilter={this.props.filterData}>{this.props.title}</button>
		)
	}
}

export default FilterButton;